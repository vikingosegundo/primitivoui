Primitivo
=========

Primitivo offers a declarative coding style for UIKit.  
It does so by breaking all tasks in small primitives, also known as filters.

```swift
public
extension UIView {
    @discardableResult func tinted(color: UIColor)   -> UIView { tintColor = color                            ; return self }
    @discardableResult func alpha(_ _alpha: CGFloat) -> UIView { alpha     = _alpha                           ; return self }
    @discardableResult func hide(_ hide: Bool)       -> UIView { alpha     = hide ? 0 : 1                     ; return self }
    @discardableResult func fit()                    -> UIView { sizeToFit()                                  ; return self }
    @discardableResult func text(_ text: String)     -> UIView { if let l = self as? UILabel { l.text = text }; return self }
}
```

Primitives start out very simple, but are chain-able, so more complex primitives can be created by chaining other primitives.

```swift
@discardableResult func filtered(color: UIColor, alpha: CGFloat) -> UIView
{
    UIView(frame: bounds)
        .add(to: self)
        .centered(on: self)
        .background(color: color, contained: false)
        .alpha(alpha)
    return self
}
```

Swift versatile type system gives us quite some choices to create a powerful DSL to express our intentions declarative.

```swift
public enum Shadow {
    case no
    case colored(color: UIColor = .black,
                 opacity: CGFloat = 0.5,
                 radius: CGFloat = 5.0,
                 offset: CGSize = .zero,
                 path: CGPath? = nil)
}

@discardableResult func shadow(_ s: Shadow = .colored()) -> UIView
{
    switch s {
    case .no: return self
    case let .colored(color: c,
                    opacity: o,
                     radius: r,
                     offset: os,
                       path: p):
        layer.shadowColor = c.cgColor
        layer.shadowOpacity = Float(o)
        layer.shadowRadius = r
        layer.shadowOffset = os
        layer.shadowPath = p
        clipsToBounds = false
        return self
    }
}
```

With some pattern magic also complex behaviour like animation are chain-able

```swift
public protocol Animation {
    func execute()
}

public
struct AnimationBuilder {
    public init() {}
    public func build(_ view: UIView, config: AnimationConfig) -> Animation
    {
        switch config {
        case let .no    (alpha: a                         ): return NoAnimation    (view: view, alpha: a)
        case let .hide  (duration:  d, delayed: dl, unhide): return HideAnimation  (view: view, hide:true,  duration: d, delayed: dl, completion: unhide)
        case let .unhide(duration:  d, delayed: dl, hide  ): return HideAnimation  (view: view, hide:false, duration: d, delayed: dl,completion: hide)
        case let .pulse (duration: pd, alpha: a, runs: nr ): return PulseAnimation (view: view, alpha: a, run: nr, pulseDuration: pd)
        case let .wobble(duration:  d, angle: a, runs: r  ): return WobbleAnimation(view: view, duration: d, angle: a, runs: r)
        case let .custom(animation: animation             ): return animation
        }
    }
}

public
indirect enum AnimationConfig{
    case no(alpha: CGFloat)
    case hide  (duration: TimeInterval = 5, delayed: TimeInterval = 0, unhide: AnimationConfig = AnimationConfig.unhide(duration: 0.4, delayed: 0, hide: AnimationConfig.no(alpha:1)))
    case unhide(duration: TimeInterval = 5, delayed: TimeInterval = 0,   hide: AnimationConfig = AnimationConfig.hide(duration: 0.4, delayed: 2, unhide: AnimationConfig.no(alpha:0)))
    case pulse (duration: TimeInterval = 0.5, alpha: CGFloat, runs:Int = 300)
    case wobble(duration: TimeInterval = 0.07, angle: CGFloat = 5.0, runs: Int = 10000)
    case custom(animation: Animation)
}

public
struct WobbleAnimation: Animation {
    public func execute() { animate(view: view, duration: duration, angle: angle, runs: runs) }

    let view: UIView
    let duration: TimeInterval
    let angle: CGFloat
    let runs: Int

    private func animate(view: UIView,
                         duration: TimeInterval,
                         angle: CGFloat,
                         runs: Int,
                         reverse: Bool = false)
    {
        if runs == -1 { view.transform = .identity; return }
        UIView.animate(withDuration: duration, delay: 0, options:[ ]) {
            view.transform = CGAffineTransform.identity.rotated(by: CGFloat(((( (reverse ? -1 : 1) * angle) * .pi) / 180.0)))
        } completion: { _ in
            animate(view: view, duration: duration, angle: angle, runs: runs - 1, reverse: !reverse)
        }
    }
}

public
extension UIView {
    @discardableResult
    func animate(_ config : AnimationConfig) -> UIView
    {
        let animation = AnimationBuilder().build(self, config: config)
        animation.execute()
        return self
    }

}

```
We can to some extend combine animations like
```Swift
.tapped {
    $0?
        .animate(.fade  (duration: 2.0,               scale:     3.0, to: 0.0))
        .animate(.fade  (duration: 1.0, delayed: 2.0, scale: 1.0/3.0, to: 1.0))
        .animate(.wobble(               delayed: 3.0, runs: 5))
}
```
to achieve  
![complexanimation].  

But currently those capabilities are very limited, though I will greatly increase them in the future. Therefore animation is under constant flux.

----

Now let's combine the things to complex behaviour.  

We what to create a primitive that displays any view it is called on as a Toast message

```swift
aview.toastify(...)
```

To implement it I had to add the `show` primitive and a private method to determine the destination view. All other primitives are reused as they had been developed before:


```swift
public enum ToastPosition {
    case    top(CGFloat)
    case  center(offset: CGPoint = .zero)
    case bottom(CGFloat)
}

public
extension UIView {

    @discardableResult
    func toastify(shape          : Shape = .rect(rounded: 10),
                  textColor      : UIColor = .white,
                  backgroundColor: UIColor = .darkGray,
                  borderColor    : UIColor = .white,
                  borderWidth    : CGFloat = 1,
                  alpha          : CGFloat = 0.8,
                  position       : ToastPosition = .top(55), on view: UIView? = nil,
                  shadow: Shadow = .colored(color: .black)) -> UIView
    {
            text(color: textColor)
            .text(alignment: .center)
            .fit()
            .shaped(shape)
            .contentInset(top: 5, right: 5, bottom: 5, left: 5)
            .bordered(borderColor, width: borderWidth)
            .background(color: backgroundColor)
            .show(at: position, on: view)
            .shadow(shadow)
    }

    @discardableResult
    func show(at position: ToastPosition,
              on view: UIView?,
              duration: TimeInterval = 1) -> UIView
    {
        translatesAutoresizingMaskIntoConstraints = true
        func y(top: CGFloat) -> CGFloat {
            let hasBar = !(window?.rootViewController?.navigationController?.navigationBar.isHidden ?? false)
            return -(destination.frame.size.height / 2 - top - ((hasBar && view != nil) ? 44 : 0)  - frame.size.height/2) + 30
        }

        let destination = getDestination(view)
        switch position {
        case .top(let top):
            centered(on: destination, offset: CGPoint(x: 0, y: y(top: top) ) )
        case .center(offset: let offset):
            centered(on: destination, offset: offset)
        case .bottom(let bottom):
            centered(on: destination, offset: CGPoint(x: 0, y: (destination.frame.size.height / 2 - bottom - frame.size.height/2) ))
        }

        return
            add(to: destination)
            .hide(true)
            .animate(.unhide(duration: duration))
    }
}

private
extension UIView {
    private // get app's window if view isn't provided
    func getDestination(_ view: UIView?) -> UIView {
        let destination: UIView
        if view != nil {
            destination = view!
        } else {
            guard
                let appWindow = (UIApplication.shared.windows.reversed().last { ($0.screen == UIScreen.main) && (!$0.isHidden && $0.alpha > 0) && ($0.windowLevel == UIWindow.Level.normal) } )
            else { return self }
            destination = appWindow
        }
        return destination
    }
}

```
toastifying a label and an avatar:  
![label_toast] ![avatar_toast]


AvatarKit
=========

Primitivo also comes with a second framework — AvatarKit — which uses Primitivo.
Although itself has less than 200 lines, it allows for highly customisable avatars, including identicons, which are created as kaleidoscope images.

![avatars][avatars]
![identicons][identicons]

The first rows avatars are generated by this:

```swift
Avatar
    .from(image: .init(imageLiteralResourceName: "m1"),
          width: 50)
    .add(to: view)
    .centered(on: view, offset: .init(x: -120, y: -260))
Avatar
    .from(image: .init(imageLiteralResourceName: "m1"),
          width: 50,
          filter: .color(.systemTeal, alpha: 0.25))
    .add(to: view)
    .centered(on: view, offset: .init(x: -40, y: -260))
Avatar
    .from(image: .init(imageLiteralResourceName: "m1"),
          shaped: .circle,
          width: 50,
          filter: .color(.systemTeal, alpha: 0.25))
    .bordered(.systemTeal, width: 2)
    .add(to: view)
    .centered(on: view, offset: .init(x: 40, y: -260))
Avatar
    .from(image: .init(imageLiteralResourceName: "m1"),
          shaped: .circle,
          shadow: .colored(color: .systemTeal, opacity: 0.75, radius: 10, offset: .init(width: 10, height: 10)),
          width: 50,
          filter: .color(.systemTeal, alpha: 0.25))
    .bordered(.systemTeal, width: 2)
    .add(to: view)
    .centered(on: view, offset: .init(x: 120, y: -260))
```

And the first row of identicons:

```swift
Avatar
    .kaleidoscope(for:"manuelmeyer".avatarHash, width: 50)
    .add(to: view)
    .centered(on: view, offset: .init(x: -120, y: -240))

Avatar
    .kaleidoscope(for: "Manuelmeyer".avatarHash, shadow: .colored(color: .systemIndigo, offset: .init(width: 5, height: 5)), width: 50)
    .bordered(.black, width: 2)
    .add(to: view)
    .centered(on: view, offset: .init(x: -40, y: -240))

Avatar
    .kaleidoscope(for: "MANUELMEYER".avatarHash, width: 50)
    .add(to: view)
    .centered(on: view, offset: .init(x: 40, y: -240))

Avatar
    .kaleidoscope(for: "MANUELMEYER".avatarHash, width: 50, saturation: 0)
    .add(to: view)
    .centered(on: view, offset: .init(x: 120, y: -240))

Avatar
    .kaleidoscope(for: "Hallo world".avatarHash, width: 50)
    .add(to: view)
    .centered(on: view, offset: .init(x: -120, y: -170))

```

Of course you would decide for one style and wrap it in a view, like

```swift
import UIKit
import AvatarKit

class AvatarView: UIView {
    var user: User? { didSet { configure() } }
    lazy
    var fallback = UIColor.lightGray.image(with: frame.size)
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configure() {
        switch (user?.image, user?.avatarHash) {
        case (nil,   nil): configure(with: fallback)
        case (nil, let h): configure(with: h!)
        case (let img, _): configure(with: img!)
        }
    }
    private func configure(with image: UIImage) {
        Avatar
            .from(image : image,
                  width : min(frame.size.width, frame.size.height),
                  filter: user != nil
                            ? .color(.color(for: user!.avatarHash), alpha: 0.25)
                            : .color(.clear))
            .add(to: viewWithSubviewsRemoved())
    }
    private func configure(with hash: Int) {
        Avatar
            .kaleidoscope(for  : hash,
                          width: min(frame.size.width, frame.size.height))
            .add(to: viewWithSubviewsRemoved())
    }
}
```
![avatarview]

The first view shows an AvatarView with an image provided by the user. In the second no image is present but a hash can be generated for the user. The third shows an AvatarView, if no user is set yet.  
AvatarView is not member in Primitivo or AvatarKit, but can be found in the PrimitivoCatalog target.

ContextMenu
-----------

Primitivo also comes with easy context menu handling

```swift
Avatar
    .from(image: .init(imageLiteralResourceName: "m1"), shaped: .square(rounded: 15))
    .add(to: view)
    .centered(on: view)
    .contextMenu(menuTitle:"show some love:",items: [
        (title:"hug"  , icon: UIImage(systemName: "heart.fill"), action: { print("hug: \($0)"   ) }),
        (title:"write", icon: UIImage(systemName: "pencil")    , action: { print("write: \($0)" ) })
    ])
```
results in

![context]

[label_toast]: .images/label_toast.png
[avatar_toast]: .images/avatar_toast.png
[avatars]: .images/avatars.png
[identicons]: .images/identicons.png
[avatarview]: .images/avatarview.png
[context]: .images/context_menu_01.png
[complexanimation]: .images/complexanimation.gif
