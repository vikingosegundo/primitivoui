//
//  UIImage+ext.swift
//  AvatarKit
//
//  Created by Manuel Meyer on 22.02.21.
//



import UIKit

#if os(iOS)
import Primitivo
#elseif os(tvOS)
import PrimitivoTV
#endif


public extension UIImage {
    func kaleidoscope() -> UIImage
    {
        let originalImage = CIImage(cgImage: (cgImage)!)
        guard
            let filter = CIFilter   (name: "CIKaleidoscope")
        else { return UIColor.black.image(with: size) }
        
        filter.setValue(originalImage, forKey: kCIInputImageKey)
        filter.setValue(CIVector(x: size.width / 2, y: size.width / 2), forKey: kCIInputCenterKey)
        filter.setValue(0, forKey: kCIInputAngleKey)
        filter.setValue(8, forKey: "inputCount")
        let context = CIContext(options:nil)
        guard
            let outputImage = filter.outputImage,
            let cgimg = context.createCGImage(outputImage, from: outputImage.extent)
        else { return UIColor.black.image(with: size) }
        
        return UIImage(cgImage: cgimg, scale: 1.0, orientation: imageOrientation)
    }
}
