import UIKit
import CoreImage

#if os(iOS)
import Primitivo
#elseif os(tvOS)
import PrimitivoTV
#endif

public enum Avatar {
    public struct Layout {
        public init(size: CGSize = CGSize(width: 120, height: 120),
                    shadow: Shadow? = .colored(),
                    backgroundColor: UIColor = .black,
                    borderColor: UIColor = .white,
                    borderWitdh: CGFloat = 1.0,
                    shape: Shape = .circle
        ) {
            self.size = size
            self.shadow = shadow
            self.backgroundColor = backgroundColor
            self.borderColor = borderColor
            self.borderWitdh = borderWitdh
            self.shape = shape
        }
        let size: CGSize
        let shadow: Shadow?
        let backgroundColor: UIColor
        let borderColor: UIColor
        let borderWitdh: CGFloat
        let shape: Shape
    }
    
    public enum Filter {
        case none
        case color(UIColor, alpha: CGFloat = 0.3)
    }
    
    public enum Configuration {
        case image(UIImage, Filter, Layout = Layout())
        case identicon(_Identicon)
        
        public
        enum _Identicon {
            case kaleidoscope(forHash: Int,
                           saturation: CGFloat = 0.5,
                            lightness: CGFloat = 0.75,
                            Layout = Layout())
        }
    }
    
    public static func make(conf: Configuration) -> UIView {
        switch conf {
        case let .image(image, filter, layout):
            return UIImageView(image: image)
                .avatarize(layout: layout,
                           filterColor:filterColor(filter),
                           filterAlpha:filterAlpha(filter))
        case .identicon(let .kaleidoscope(forHash: hash, sat, light, layout)):
            return UIImageView(image: kaleidoscope(for: hash, width: layout.size.width, saturation: sat, lightness: light))
                .background(color: layout.backgroundColor, contained: false)
                .avatarize(layout: layout)
        }
    }
    
    public static func from(image: UIImage,
                            shaped: Shape = .circle,
                            shadow: Shadow = .colored(),
                            width: CGFloat = 120.0,
                            filter: Filter = .none) -> UIView
    {
        make(conf: .image(image,
                          filter,
                          Layout(size:.init(width: width, height: width),
                                 shadow: shadow,
                                 shape: shaped)))
    }
    
    public static func kaleidoscope(for hash: Int,
                                  shadow    : Shadow = .colored(),
                                  width     : CGFloat = 120.0,
                                  saturation: CGFloat = 0.5,
                                  lightness : CGFloat = 0.75) -> UIView
    {
        make(conf: .identicon(.kaleidoscope(forHash: hash,
                              saturation: saturation,
                              lightness : lightness,
                              Layout(size   :.init(width: width, height: width),
                                     shadow : shadow,
                                     shape  : .circle ))))
    }
}

public extension UIView { // avatar
    @discardableResult
    func avatarize(
             layout: Avatar.Layout,
        filterColor: UIColor = .clear,
        filterAlpha: CGFloat = 1.0) -> UIView
    {
        frame = CGRect(origin: frame.origin, size: layout.size)
        filtered(color: filterColor, alpha: filterAlpha)
            .bordered(layout.borderColor, width: layout.borderWitdh)
            .shaped(layout.shape)
        let outerView = UIView(frame: frame)
            .shaped(layout.shape)
        add(to: outerView)
        frame = outerView.bounds
        return layout.shadow != nil ? outerView.shadow(layout.shadow!) : outerView
    }
}

private extension Avatar {
    private static func path(for hash: Int,
                             width: CGFloat,
                             pointCount: UInt = 48)  -> UIBezierPath
    {
        precondition(pointCount > 3, "give us something to work with")
        let points = (0..<pointCount)
            .map { (hash >> $0) & 1 }           // shift hash and take least significant bit -> {0,1}
            .enumerated()
            .reduce([]) {
                $1.element == 0
                    ? $0 + [$1.offset]          // append index
                    :      [$1.offset] + $0 }   // prepend index
            .map { CGFloat($0) * 15.21 }        // angles for index.
            .map { CGPoint.pointOnCircle(center: .init(x: width / 2, y: width / 2), radius: width / 2, angle: CGFloat($0))}
        return UIBezierPath()
            .createLines(between: points)
    }
    
    private static func kaleidoscope(  for hash: Int,
                                          width: CGFloat,
                                     saturation: CGFloat = 0.5,
                                      lightness: CGFloat = 0.5) -> UIImage
    {
        let img = UIGraphicsImageRenderer( size: CGSize(width: width, height: width))
            .image { _ in
                path(for: hash, width: width)
                    .fill(with: .white) }
            .kaleidoscope()
        return UIColor
            .color(for: hash, saturation: saturation, lightness: lightness)
            .image(with: img.size)
            .mask(with: img)
    }
}

private
extension Avatar {
    static func filterColor(_ filter: Filter) -> UIColor {
        switch filter {
        case .color(let c, _): return c
        case .none:            return .clear
        }
    }
    
    static func filterAlpha(_ filter: Filter) -> CGFloat {
        switch filter {
        case .color(_, let a): return a
        case .none:            return 1.0
        }
    }
}
