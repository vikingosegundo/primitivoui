//
//  UIView+anim.swift
//  Primitivo
//
//  Created by Manuel Meyer on 22.02.21.
//
import UIKit

public protocol Animation {
    func execute()
}

public
struct AnimationBuilder {
    public init() {}
    public func build(_ view: UIView, config: AnimationConfig) -> Animation
    {
        switch config {
        case let .no       (alpha                         ): return NoAnimation       (view:view,                                     alpha:alpha                  )
        case let .pulse    (duration, alpha,          runs): return PulseAnimation    (view:view, duration:duration,                  alpha:alpha, runs:runs       )
        case let .wobble   (duration, delayed, angle, runs): return WobbleAnimation   (view:view, duration:duration,angle:angle, runs:runs, delayed: delayed       )
        case let .hide     (duration, delayed, unhide     ): return HideAnimation     (view:view, duration:duration, delayed:delayed, hide:true,  completion:unhide)
        case let .unhide   (duration, delayed, hide       ): return HideAnimation     (view:view, duration:duration, delayed:delayed, hide:false, completion:  hide)
        case let .fade(duration, delayed, scale, to  ): return FadeScaleAnimation(view:view, duration:duration, delayed:delayed, scale:scale, alpha: to)
        case let .custom   (animation: animation          ): return animation
        }
    }
}

public
indirect enum AnimationConfig{
    case no    (alpha: CGFloat)
    case pulse (duration: TimeInterval = 0.5,                              alpha: CGFloat,       runs: Int = 300)
    case wobble(duration: TimeInterval = 0.07, delayed: TimeInterval = 0,  angle: CGFloat = 5.0, runs: Int = 10000)
    case hide  (duration: TimeInterval = 5,    delayed: TimeInterval = 0, unhide: AnimationConfig = .unhide(duration: 0.4, delayed: 0,   hide: .no(alpha:1)))
    case unhide(duration: TimeInterval = 5,    delayed: TimeInterval = 0,   hide: AnimationConfig =   .hide(duration: 0.4, delayed: 2, unhide: .no(alpha:0)))
    case fade  (duration: TimeInterval,        delayed: Double = 0.0,      scale: CGFloat, to:CGFloat)
    case custom(animation: Animation)
}

struct NoAnimation: Animation {
    func execute() { view.alpha = alpha }
    let view: UIView
    let alpha: CGFloat
}

struct WobbleAnimation: Animation {
    func execute() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(delayed * 1000))) {
            animate(view: view, duration: duration, angle: angle, runs: runs)
        }
    }
    
    let view: UIView
    let duration: TimeInterval
    let angle: CGFloat
    let runs: Int
    let delayed: Double

    private func animate(view: UIView,
                         duration: TimeInterval,
                         angle: CGFloat,
                         runs: Int,
                         reverse: Bool = false)
    

    {
        

        if runs == -1 { view.transform = .identity; return }
        
            UIView.animate(withDuration: duration, delay: 0, options:[ ]) {
                view.transform = CGAffineTransform.identity.rotated(by: CGFloat(((( (reverse ? -1 : 1) * angle) * .pi) / 180.0)))
            } completion: { _ in
                animate(view: view, duration: duration, angle: angle, runs: runs - 1, reverse: !reverse)
            }
        }
    
}

struct PulseAnimation: Animation {
    func execute() {
        animate(view: view, timesCalled: 0, idx: idx, alpha: alpha, numberRuns: run)
    }
    init(view: UIView, duration: TimeInterval = 0.5, alpha: CGFloat, runs: Int) {
        self.view = view
        self.idx = 0
        self.alpha = alpha
        self.run = runs
        self.origAlpha = view.alpha
        self.pulseDuration = duration
    }
    
    private let view: UIView
    private let idx: Int
    private let alpha: CGFloat
    private let run: Int
    private let pulseDuration: TimeInterval
    private var origAlpha: CGFloat? = nil
    
    private func animate(view           : UIView,
                         timesCalled    : Int,
                         idx            : Int,
                         alpha          : CGFloat,
                         numberRuns run : Int)
    {
        if run == -1 { view.alpha = origAlpha ?? 1.0; return }
        UIView.animate(withDuration: TimeInterval(pulseDuration), delay: 0.4, options: .curveEaseIn) {
            view.alpha = alpha
        } completion: { _ in
            UIView.animate(withDuration: TimeInterval(pulseDuration), delay: 0.4, options: .curveEaseOut) {
                view.alpha = ((run % 2 == 1) ? alpha : origAlpha) ?? 0
            } completion: { _ in
                self.animate(view: view, timesCalled: timesCalled + 1, idx: idx, alpha: alpha, numberRuns: run - 1)
            }
        }
    }
}

struct FadeScaleAnimation: Animation {
    init(view: UIView, duration: Double, delayed: Double = 0.0, scale: CGFloat, alpha: CGFloat) {
        self.view = view
        self.scale = scale
        self.alpha = alpha
        self.duration = duration
        self.delayed = delayed
    }
    func execute() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(delayed * 1000))) {
            UIView.animate(withDuration: duration, delay: 0) {
                view.transform = view.transform.scaledBy(x: scale, y: scale)
                view.alpha = alpha
            }
        }
    }
    
    let view: UIView
    let scale: CGFloat
    let alpha: CGFloat
    let duration: Double
    let delayed: Double
}

struct HideAnimation: Animation {
    func execute() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(delayed * 1000))) {
            UIView.animate(withDuration: duration, delay: 0) {
                view.alpha = hide ? 0 : 1
            } completion: { _ in
                switch self.completion {
                case is NoAnimation: view.alpha == 0 ? view.removeFromSuperview() : ()
                default: completion.execute()
                }
            }
        }
    }
    
    init(builder: AnimationBuilder = .init(), view: UIView, duration: TimeInterval = 0.5, delayed: TimeInterval, hide: Bool, completion: AnimationConfig) {
        self.builder    = builder
        self.view       = view
        self.duration   = duration
        self.hide       = hide
        self.delayed    = delayed
        self.completion = builder.build(view, config: completion)
    }
    
    private let view      : UIView
    private let duration  : TimeInterval
    private let hide      : Bool
    private let delayed   : TimeInterval
    private let completion: Animation
    private let builder   : AnimationBuilder
}


public
extension UIView {
    @discardableResult
    func animate(_ config : AnimationConfig) -> UIView {
        AnimationBuilder()
            .build(self, config: config)
            .execute()
        return self
    }
}
