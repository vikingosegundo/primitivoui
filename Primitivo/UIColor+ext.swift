//
//  UIColor+ext.swift
//  Primitivo
//
//  Created by Manuel Meyer on 22.02.21.
//

import UIKit

public extension UIColor {
    static func random(alpha a: CGFloat = 1.0) -> UIColor { .init(hue: (CGFloat(arc4random()).truncatingRemainder(dividingBy: 255.0) / 255.0), saturation: 1, brightness: 1, alpha: a) }
}

public extension UIColor {
    typealias RGBA = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)

    var rgbaValues: RGBA  {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r,
                    green: &g,
                    blue : &b,
                    alpha: &a
        )
        return (red:r, green:g, blue:b, alpha: a)
    }

    convenience init(hue        h: CGFloat,
                     saturation s: CGFloat,
                     lightness  l: CGFloat,
                     alpha      a: CGFloat)
    {
        let v = UIColor
            .from(hue       : h,
                  saturation: s,
                  lightness : l,
                  alpha     : a)
            .rgbaValues
        self.init(red   : v.red,
                  green : v.green,
                  blue  : v.blue,
                  alpha : v.alpha
        )
    }

    convenience init(hue        h: CGFloat,
                     saturation s: CGFloat,
                     lightness  l: CGFloat)
    {
        self.init(hue       : h,
                  saturation: s,
                  lightness : l,
                  alpha     : 1.0
        )
    }
    
    static func from(hue h: CGFloat, saturation s: CGFloat, lightness l: CGFloat, alpha a: CGFloat = 1.0) -> UIColor
    {
        precondition(0...1 ~= h && 0...1 ~= s && 0...1 ~= l && 0...1 ~= a, "input range must be in 0...1" )
        let h6: CGFloat = (h * 6.0).truncatingRemainder(dividingBy: 6.0)

        let c: CGFloat = (1 - abs((2 * l) - 1)) * s
        let x: CGFloat = c * (1 - abs(h6.truncatingRemainder(dividingBy: 2) - 1))
        let m: CGFloat = l - (c / 2)
        
        func rgba(from r: CGFloat, _ g: CGFloat, _ b: CGFloat) -> RGBA { (red: r+m, green: g+m, blue: b+m, alpha: a) }
        func color(from rgba: RGBA) -> UIColor { .init(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha: rgba.alpha) }
        
        switch h6 {
        case 0..<1: return color(from: rgba(from: c, x, 0 ))
        case 1..<2: return color(from: rgba(from: x, c, 0 ))
        case 2..<3: return color(from: rgba(from: 0, c, x ))
        case 3..<4: return color(from: rgba(from: 0, x, c ))
        case 4..<5: return color(from: rgba(from: x, 0, c ))
        case 5..<6: return color(from: rgba(from: c, 0, x ))
        default:    return .black
        }
    }
}

public extension UIColor {
     func image(with size: CGSize = CGSize(width: 1, height: 1)) -> UIImage
     {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}

public extension UIColor {
    static func color(for hash: Int, saturation: CGFloat = 0.5, lightness: CGFloat = 0.5, divider: Int = 32) -> UIColor {
        UIColor(
            hue       : CGFloat((abs(hash) % divider)) / CGFloat(divider),
            saturation: saturation,
            lightness : lightness
        )
    }
    
    static func color(for hash: Int, saturation: CGFloat = 0.5, lightness: CGFloat = 0.5, palette: [UIColor]) -> UIColor {
        palette[(abs(hash) % palette.count) ]
    }
}
