//
//  UIView+contextMenu.swift
//  Primitivo
//
//  Created by Manuel Meyer on 09.03.21.
//

import UIKit

public
extension UIView {
    @discardableResult
    func contextMenu( menuTitle: String = "", items: [(title: String, icon: UIImage?, action: (UIView) -> ())]) -> UIView
    {
        let builder = ContextMenuBuilder(view: self, menuTitle: menuTitle, items: items)
        ContextMenuRegistry.register(builder: builder)
        addInteraction(UIContextMenuInteraction(delegate: builder))
        return self
    }
}

fileprivate
class ContextMenuBuilder: NSObject, UIContextMenuInteractionDelegate {
    init(view: UIView?, menuTitle: String, items: [(title: String, icon: UIImage?, action: (UIView) -> ())]) {
        self.view = view
        self.menuTitle = menuTitle
        self.items = items
        super.init()
    }
    
    func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil,
                                          previewProvider: nil) { [weak self] _ in
            UIMenu(title   : self?.menuTitle ?? "",
                   children: (self?.items ?? []).map { item in UIAction(title: item.title, image: item.icon, handler: { _ in item.action(self?.view ?? UIView())}) }
            )
        }
    }
    
    weak
    fileprivate var view: UIView?
    fileprivate let items: [(title: String, icon: UIImage?, action: (UIView) -> ())]
    fileprivate let menuTitle: String
}


public
enum ContextMenuRegistry {
    static public func flush() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(5)) { builders = builders.filter { $0.view != nil } }
    }
    
    static private var builders:[ContextMenuBuilder] = []
    static fileprivate func register(builder: ContextMenuBuilder) {
        builders = (builders + [builder]).filter { $0.view != nil }
    }
}
