//
//  UIBezierPath+ext.swift
//  Primitivo
//
//  Created by Manuel Meyer on 22.02.21.
//

import UIKit

public extension UIBezierPath {
    @discardableResult func fill       (with color: UIColor)       -> UIBezierPath { color.setFill(); fill()        ; return self }
    @discardableResult func moveTo     (_ point: CGPoint)          -> UIBezierPath { move(to: point)                ; return self }
    @discardableResult func addLineTo  (_ point: CGPoint)          -> UIBezierPath { addLine(to: point)             ; return self }
    @discardableResult func createLines(between points: [CGPoint]) -> UIBezierPath { move(to: points.first ?? .zero); return addLines(between: points) }
    @discardableResult func addLines   (between points: [CGPoint]) -> UIBezierPath { points.dropFirst().reduce(self) { $0.addLineTo($1) } }
}
