import UIKit

public extension UIImage {
    func tinted(with tintColor: UIColor) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height) as CGRect
        context.clip(to: rect, mask: cgImage!)
        tintColor.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return newImage
    }
    
    func rotate(angle: CGFloat) -> UIImage
    {
        let radians = CGFloat(angle * .pi) / 180.0
        let image = UIGraphicsImageRenderer(size:size).image {
            let context = $0.cgContext
            context.translateBy(x: size.width/2, y: size.height/2)
            context.rotate(by: radians)
            draw(in: CGRect(origin: CGPoint(x: -size.width/2, y: -size.height/2), size: size))
        }
        return image
    }

    func mask(with mask: UIImage) -> UIImage {
        UIGraphicsImageRenderer( size: CGSize(width: size.width, height: size.height))
            .image { context in
                context.cgContext.translateBy(x: 0.0, y: size.height)
                context.cgContext.scaleBy(x: 1.0, y: -1.0)
                context.cgContext.clip(to: CGRect(origin: .zero, size: size), mask: mask.cgImage!)
                context.cgContext.draw(cgImage!, in: CGRect(origin: .zero, size: size))
            }
    }
}
