//
//  CGPoint+ext.swift
//  Primitivo
//
//  Created by Manuel Meyer on 22.02.21.
//

import UIKit

public extension CGPoint {
    static func pointOnCircle(center: CGPoint, radius: CGFloat, angle: CGFloat) -> CGPoint {
        CGPoint(
            x: center.x + radius * cos(angle),
            y: center.y + radius * sin(angle)
        )
    }
}
