//
//  Toast.swift
//  Primitivo
//
//  Created by Manuel Meyer on 02.03.21.
//

import UIKit

public enum ToastPosition {
    case    top(CGFloat)
    case center(offset: CGPoint = .zero)
    case bottom(CGFloat)
}

public
extension UIView {
    @discardableResult
    func toastify(shape          : Shape = .rect(rounded: 10),
                  textColor      : UIColor = .white,
                  backgroundColor: UIColor = .darkGray,
                  borderColor    : UIColor = .white,
                  borderWidth    : CGFloat = 1,
                  alpha          : CGFloat = 0.8,
                  at position    : ToastPosition = .top(55),
                  on view        : UIView? = nil,
                  shadow         : Shadow = .colored(color: .black)) -> UIView
    {
            text(color: textColor)
            .text(alignment: .center)
            .fit()
            .shaped(shape)
            .contentInset(top: 5, right: 5, bottom: 5, left: 5)
            .bordered(borderColor, width: borderWidth)
            .background(color: backgroundColor)
            .show(at: position, on: view)
            .shadow(shadow)
    }
    
    @discardableResult
    func show(at position: ToastPosition,
              on view: UIView?,
              duration: TimeInterval = 1) -> UIView
    {
        translatesAutoresizingMaskIntoConstraints = true
        func y(top: CGFloat) -> CGFloat {
            let hasBar = !(window?.rootViewController?.navigationController?.navigationBar.isHidden ?? false)
            return -(destination.frame.size.height / 2 - top - ((hasBar && view != nil) ? 44 : 0)  - frame.size.height/2) + 30
        }
        
        let destination = getDestination(view)
        switch position {
        case .top(let top              ): centered(on: destination, offset: CGPoint(x: 0, y: y(top: top) ) )
        case .center(offset: let offset): centered(on: destination, offset: offset                         )
        case .bottom(let bottom        ): centered(on: destination, offset: CGPoint(x: 0, y: (destination.frame.size.height / 2 - bottom - frame.size.height/2) ))
        }

        return
            add(to: destination)
            .hide(true)
            .animate(.unhide(duration: duration))
    }
}

private
extension UIView {
    private // get app's window if view isn't provided
    func getDestination(_ view: UIView?) -> UIView {
        let destination: UIView
        if view != nil {
            destination = view!
        } else {
            guard
                let appWindow = (UIApplication.shared.windows.reversed().last { ($0.screen == UIScreen.main) && (!$0.isHidden && $0.alpha > 0) && ($0.windowLevel == UIWindow.Level.normal) } )
            else { return self }
            destination = appWindow
        }
        return destination
    }
}
