import UIKit

public enum Button{
    public static func with(title: String,
                            size: CGSize = .init(width: 100, height: 44),
                            textColor: UIColor = .black,
                            backgroundColor: UIColor = .orange,
                            rounded r: CGFloat = 5.0) -> UIButton
    {
        UIButton(frame: CGRect(origin: .zero, size: size))
            .title(title)
            .background(color: backgroundColor)
            .shaped(.rect(rounded: r))
            .shadow()
            .text(color: textColor)
            .tapped(
                action: { print("tapped: \(String(describing: $0))") }
            ) as! UIButton
    }
}
