//
//  UIView+filter.swift
//  Primitivo
//
//  Created by Manuel Meyer on 22.02.21.
//

import UIKit

public extension UIView { // Color
    @discardableResult func tinted(color: UIColor)           -> UIView { tintColor = color                                          ; return self }
    @discardableResult func alpha(_ _alpha: CGFloat)         -> UIView { alpha     = _alpha                                         ; return self }
    @discardableResult func hide(_ hide: Bool)               -> UIView { alpha     = hide ? 0 : 1                                   ; return self }
    @discardableResult func fit()                            -> UIView { sizeToFit()                                                ; return self }
    @discardableResult func text(_ text: String)             -> UIView { if let l = self as? UILabel { l.text = text }              ; return self }
    @discardableResult func text(alignment: NSTextAlignment) -> UIView { if let l = self as? UILabel { l.textAlignment = alignment }; return self }
    
    @discardableResult func background(color: UIColor, contained: Bool = true) -> UIView {
        func contain(_ v: UIView) -> UIView {
            let container = UIView(frame: self.frame)
            container.backgroundColor = .clear
            self.frame = container.bounds
            add(to: container)
            return container
        }
        clipsToBounds = true
        backgroundColor = color
        return contained ? contain(self) : self
    }

    @discardableResult func filtered(color: UIColor, alpha: CGFloat) -> UIView
    {
        UIView(frame: bounds)
            .add(to: self)
            .centered(on: self)
            .background(color: color, contained: false)
            .alpha(alpha)
        return self
    }
    @discardableResult func text(color: UIColor) -> UIView {
        switch self {
        case is UIButton: (self as! UIButton).setTitleColor(color, for: .normal)
        case is UILabel: (self as! UILabel).textColor = color
        default: break
        }
        return self
    }
}

public enum Shadow {
    case no
    case colored(color: UIColor = .black,
                 opacity: CGFloat = 0.5,
                 radius: CGFloat = 5.0,
                 offset: CGSize = .zero,
                 path: CGPath? = nil)
}

public extension UIView { // Shadow
    @discardableResult func shadow(_ s: Shadow = .colored()) -> UIView
    {
        switch s {
        case .no: return self
        case let .colored(color: c,
                        opacity: o,
                         radius: r,
                         offset: os,
                           path: p):
            layer.shadowColor = c.cgColor
            layer.shadowOpacity = Float(o)
            layer.shadowRadius = r
            layer.shadowOffset = os
            layer.shadowPath = p
            clipsToBounds = false
            return self
        }
    }
}

public extension UIView { // border
    @discardableResult func bordered(_ color: UIColor, width: CGFloat = 1.0) -> UIView { configureBorder(color: color, width: width) }
}

public extension UIView { // actions
    @discardableResult func tapped(action fun: @escaping (UIView?) -> ()) -> UIView {
        func control(for view: UIView) { (view as! UIControl).addAction(for: .touchUpInside) { fun($0) } }
        func gesture(for view: UIView) { addGestureRecognizer(BindableGestureRecognizer(action: fun)); isUserInteractionEnabled = true }
        switch self {
        case is UIControl: control(for: self)
        default          : gesture(for: self)
        }
        return self
    }
}

public enum Shape {
    case circle
    case rect(rounded: CGFloat = 0.0)
    case square(rounded: CGFloat = 0.0)
    case custom(path: UIBezierPath)
}

public extension UIView { // shapes
    @discardableResult func add(to view: UIView)                               -> UIView { view.addSubview(self); return self }
    @discardableResult func centered(on view: UIView, offset: CGPoint = .zero) -> UIView { frame = frame.offsetBy(dx: (view.frame.size.width / 2 - frame.size.width / 2 + offset.x), dy: (view.frame.size.height / 2 - frame.size.height / 2) + offset.y); return self }
    @discardableResult func resize()                                           -> UIView { sizeToFit(); return self }
    @discardableResult func shaped(_ shape: Shape)                             -> UIView { configure(shape: shape) }
}

public extension UIView { // button stuff
    @discardableResult func contentInset(top t: CGFloat = 0,
                                         right r: CGFloat = 0,
                                         bottom b: CGFloat = 0,
                                         left l: CGFloat = 0) -> UIView
    {
        if let bt = self as? UIButton { bt.contentEdgeInsets = .init(top: t, left: l, bottom: b, right: r)}
        if let la = self as? UILabel { la.frame = CGRect(origin: la.frame.origin, size: CGSize(width: la.frame.size.width + l + r, height: la.frame.size.height + t + b))}
        return self
    }
    
    @discardableResult func title(_ title: String, textColor tc: UIColor = .black) -> UIView {
        if let bt = self as? UIButton {
            bt.setTitle(title, for: .normal)
        }
        return text(color: tc)
        
    }
}

public extension UIView { // tools
    @discardableResult func viewWithSubviewsRemoved() -> UIView { subviews.forEach { $0.removeFromSuperview() }; return self }
}


private final class BindableGestureRecognizer: UITapGestureRecognizer {
    @discardableResult
    init(action: @escaping (UIView?) -> Void) {
        self.action = action
        super.init(target: nil, action: nil)
        self.addTarget(self, action: #selector(execute))
    }
    private var action: (UIView?) -> Void
    @objc private func execute() { action(self.view) }
}

@objc private class ClosureSleeve: NSObject {
    init (_ _closure: @escaping (UIView?)->()) { closure = _closure }
    @objc func invoke (view: UIView?) { closure(view) }
    private let closure: (UIView?)->()
}

private extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .touchUpInside, _ closure: @escaping (UIView?)->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}

private extension UIView {
    private func configure(shape: Shape) -> UIView {
        switch shape {
        case .circle:
            layer.cornerRadius = min(frame.size.width, frame.size.height) / 2
        case .rect(rounded: let r):
            layer.cornerRadius = CGFloat(r)
        case .square(rounded: let r):
            let x = min(frame.size.width, frame.size.height)
            frame = CGRect(origin: frame.origin, size: CGSize(width: x, height: x))
            layer.cornerRadius = CGFloat(r)
        case .custom(path: let p):
            let l = CAShapeLayer()
            l.path = p.cgPath
            layer.mask = l
        }
        clipsToBounds = true
        return self
    }
    
    private func configureBorder(color: UIColor, width: CGFloat ) -> UIView {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        return self
    }
}
