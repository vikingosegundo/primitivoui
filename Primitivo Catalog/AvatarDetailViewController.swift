//
//  AvatarDetailViewController.swift
//  PrimitivoUI
//
//  Created by Manuel Meyer on 21.02.21.
//

import UIKit

class AvatarDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let id = UUID()
        
        AvatarView(frame: CGRect(origin: .zero, size: CGSize(width: 180, height: 180)))
            .user(User(id: id, name: "", image: .init(imageLiteralResourceName: "m1")))
            .add(to: view)
            .centered(on: view, offset: .init(x: 0, y: -200))
        
        AvatarView(frame: CGRect(origin: .zero, size: CGSize(width: 180, height: 180)))
            .user(User(id: id, name: "", image: nil))
            .add(to: view)
            .centered(on: view)
        
        AvatarView(frame: CGRect(origin: .zero, size: CGSize(width: 180, height: 180)))
            .add(to: view)
            .centered(on: view, offset: CGPoint(x: 0, y: 200))
    }

}
