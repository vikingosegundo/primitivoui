//
//  IdenticonContactSheetViewController.swift
//  PrimitivoUI
//
//  Created by Manuel Meyer on 21.02.21.
//

import UIKit
import AvatarKit

class IdenticonContactSheetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Avatar
            .kaleidoscope(for:"manuelmeyer".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: -120, y: -240))

        Avatar
            .kaleidoscope(for: "Manuelmeyer".avatarHash, shadow: .colored(color: .systemIndigo, offset: .init(width: 5, height: 5)), width: 50)
            .bordered(.black, width: 2)
            .add(to: view)
            .centered(on: view, offset: .init(x: -40, y: -240))

        Avatar
            .kaleidoscope(for: "MANUELMEYER".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: 40, y: -240))

        Avatar
            .kaleidoscope(for: "MANUELMEYER".avatarHash, width: 50, saturation: 0)
            .add(to: view)
            .centered(on: view, offset: .init(x: 120, y: -240))

        Avatar
            .kaleidoscope(for: "Hallo world".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: -120, y: -170))

        Avatar
            .kaleidoscope(for: "Hallo world!".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: -40, y: -170))

        Avatar
            .kaleidoscope(for: "Hello world".avatarHash, shadow: .no, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: 40, y: -170))

        Avatar
            .kaleidoscope(for: "Hello world".avatarHash, width: 50, saturation: 0, lightness: 0.8)
            .add(to: view)
            .centered(on: view, offset: .init(x: 120, y: -170))
        
        Avatar
            .kaleidoscope(for: "hello world!".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: -120, y: -100))

        Avatar
            .kaleidoscope(for: "hello, world!".avatarHash, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: -40, y: -100))

        Avatar
            .kaleidoscope(for: "Hello, World!".avatarHash, shadow: .no, width: 50)
            .add(to: view)
            .centered(on: view, offset: .init(x: 40, y: -100))

        Avatar
            .kaleidoscope(for: "Hello, World!".avatarHash, width: 50, saturation: 0, lightness: 0.8)
            .add(to: view)
            .centered(on: view, offset: .init(x: 120, y: -100))
        
        Avatar
            .kaleidoscope(for:"manuelmeyer".avatarHash)
            .add(to: view)
            .centered(on: view, offset: .init(x: -80, y: 0))
        
        Avatar
            .kaleidoscope(for: "Manuelmeyer".avatarHash)
            .bordered(.color(for: "Manuelmeyer".avatarHash), width: 1)
            .add(to: view)
            .centered(on: view, offset: .init(x: 80, y: 0))

        Avatar
            .kaleidoscope(for: "Hallo world!".avatarHash)
            .add(to: view)
            .centered(on: view, offset: .init(x: -80, y: 140))

        Avatar
            .kaleidoscope(for: "MANUELMEYER".avatarHash)
            .add(to: view)
            .centered(on: view, offset: .init(x: 80, y: 140))
    }
}
