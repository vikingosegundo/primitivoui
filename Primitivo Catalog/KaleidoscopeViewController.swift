//
//  KaleidoscopeViewController.swift
//  Primitivo Catalog
//
//  Created by Manuel Meyer on 03.03.21.
//

import UIKit
#if os(iOS)
import AvatarKit
#elseif os(tvOS)
import AvatarKitTV
#endif

class KaleidoscopeViewController: UIViewController {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.hide(true)
        view.background(color: .black)
        NotificationCenter.default.addObserver(self, selector: #selector(activate), name: UIApplication.didBecomeActiveNotification, object: nil)
        nextKaleidoscope(hash: .random(in: 0..<1_000_000))
    }
    
    private func nextKaleidoscope(hash: Int) {
        Avatar
            .kaleidoscope(for: hash, width: min(view.frame.size.width, view.frame.size.height) * 4/5)
            .toastify(shape    : .circle,
                    borderColor: .color(for: hash, saturation: 0.2),
                    borderWidth: 3,
                    at         : .center(),
                    on         : view,
                    shadow     : .no)

        if UIApplication.shared.applicationState == .active {
            lastHash = hash
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000)) { [weak self] in self?.nextKaleidoscope(hash: hash + 5) } // 250 ..1250
        }
    }

    var lastHash = 0
    @objc private func activate(_ noti: Notification) {
        navigationController?.navigationBar.hide(true)
        self.nextKaleidoscope(hash: lastHash + 5)
    }
}
