//
//  ToastSheetViewController.swift
//  Primitivo Catalog
//
//  Created by Manuel Meyer on 02.03.21.
//

import UIKit
import Primitivo
import AvatarKit

class ToastSheetViewController: UIViewController {
    
    @IBOutlet weak var type: UISegmentedControl!
    @IBOutlet weak var position: UISegmentedControl!
    @IBOutlet weak var shadowSelection: UISegmentedControl!
    @IBOutlet weak var clickMe: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        clickMe
            .tapped(action: showToast)
    }
}

private
extension ToastSheetViewController {
    func showToast(sender:UIView?) {
        let pos = getPosition()
        let toast: UIView
        switch type.selectedSegmentIndex {
        case 0: toast = showLabel(position: pos)
        case 1: toast = showAvatar(position: pos)
        case 2: toast = showIdenticon(position: pos)
        default: toast = UIView()
        }
        toast.tapped {
            $0?.animate(.fade(duration: 3.0, scale: 3.0, to: 0))
        }
    }
        
    func showLabel(position: ToastPosition) -> UIView  {
        UILabel(frame: .zero)
            .text("Hello, World!")
            .toastify(at:position, shadow: shadow())
    }
    
    func showAvatar(position: ToastPosition) -> UIView {
        Avatar
            .from(image : .init(imageLiteralResourceName: "m1"),
                  shaped: .square(),
                  width : 150)
            .toastify(at    : position,
                      shadow: shadow())
    }
    
    func showIdenticon(position: ToastPosition) -> UIView {
        AvatarView(frame: CGRect(origin: .zero, size: CGSize(width: 150, height: 150)))
            .user(User(name: "doobedoo", image: nil))
            .toastify(shape : .circle,
                      at    : position,
                      shadow: shadow())
    }
    
    func getPosition() -> ToastPosition {
        let pos: ToastPosition
        switch position.selectedSegmentIndex {
        case 0 : pos = .top(15)
        case 1 : pos = .center(offset: CGPoint(x: 0, y: -100))
        case 2 : pos = .bottom(25)
        default: pos = .top(0)
        }
        return pos
    }
    
    func shadow() -> Shadow {
        let s: Shadow
        switch shadowSelection.selectedSegmentIndex {
        case 0 : s = .colored(color: .black)
        case 1 : s = .colored(color: .systemTeal, opacity: 0.8, radius: 30)
        case 2 : s = .no
        default: s = .no
        }
        return s
    }
}

extension UIView {
    @discardableResult public func user(_ user: User) -> UIView {
        if let l = self as? AvatarView { l.user = user }
        return self
    }
}
