//
//  AvatarView.swift
//  PrimitivoUI
//
//  Created by Manuel Meyer on 21.02.21.
//

import UIKit
import AvatarKit

class AvatarView: UIView {
    var user: User? { didSet { configure() } }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        switch (user?.image, user?.avatarHash) {
        case     (.none,   .none   ): configure(with: UIColor.lightGray.image(with: frame.size))
        case let (.none,   .some(h)): configure(with: h)
        case let (.some(i),       _): configure(with: i)
        }
    }
    private func configure(with image: UIImage) {
        Avatar
            .from(image : image,
                  width : min(frame.size.width, frame.size.height),
                  filter: user != nil
                            ? .color(.color(for: user!.avatarHash), alpha: 0.25)
                            : .color(.clear))
            .add(to: viewWithSubviewsRemoved())
    }
    private func configure(with hash: Int) {
        Avatar
            .kaleidoscope(for  : hash,
                          width: min(frame.size.width, frame.size.height))
            .add(to: viewWithSubviewsRemoved())
    }
}
