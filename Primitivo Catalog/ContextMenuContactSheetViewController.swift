//
//  ContextMenuContactSheetViewController.swift
//  Primitivo Catalog
//
//  Created by Manuel Meyer on 09.03.21.
//

import UIKit
import AvatarKit
import Primitivo

class ContextMenuContactSheetViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        Avatar
            .from(image: UIImage(imageLiteralResourceName: "m1"), shaped: .square(rounded: 10))
            .add(to: view)
            .centered(on: view)
            .contextMenu(menuTitle:"show some love:", items: [
                (title:"hug"  , icon: UIImage(systemName: "heart.fill"), action: { v in print("hug:   \(v)"); v.animate(.wobble(duration: 0.25,              runs: 7)) }),
                (title:"write", icon: UIImage(systemName: "pencil")    , action: { v in print("write: \(v)"); v.animate(.pulse (duration: 0.15, alpha: 0.75, runs: 5)) })
            ])
            .tapped {
                $0?
                    .animate(.fade  (duration: 2.0,               scale:     3.0, to: 0.0))
                    .animate(.fade  (duration: 1.0, delayed: 2.0, scale: 1.0/3.0, to: 1.0))
                    .animate(.wobble(               delayed: 3.0, runs: 5))
            }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ContextMenuRegistry.flush()
    }
}

