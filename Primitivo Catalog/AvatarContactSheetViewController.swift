//
//  AvatarContactSheetViewController.swift
//  PrimitivoUI
//
//  Created by Manuel Meyer on 21.02.21.
//

import UIKit
import AvatarKit

class AvatarContactSheetViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure() {
        DispatchQueue.main.async { [self] in
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      width: 50)
                .add(to: view)
                .centered(on: view, offset: .init(x: -120, y: -260))
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      width: 50,
                      filter: .color(.systemTeal, alpha: 0.25))
                .add(to: view)
                .centered(on: view, offset: .init(x: -40, y: -260))
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .circle,
                      width: 50,
                      filter: .color(.systemTeal, alpha: 0.25))
                .bordered(.systemTeal, width: 2)
                .add(to: view)
                .centered(on: view, offset: .init(x: 40, y: -260))
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .circle,
                      shadow: .colored(color: .systemTeal, opacity: 0.75, radius: 10, offset: .init(width: 10, height: 10)),
                      width: 50,
                      filter: .color(.systemTeal, alpha: 0.25))
                .bordered(.systemTeal, width: 2)
                .add(to: view)
                .centered(on: view, offset: .init(x: 120, y: -260))
            
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .rect(rounded: 8),
                      width: 50)
                .add(to: view)
                .centered(on: view, offset: .init(x: -120, y: -180))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .square(rounded: 8),
                      width: 50,
                      filter: .color(.systemTeal, alpha: 0.25))
                .add(to: view)
                .centered(on: view, offset: .init(x: -40, y: -180))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .square(rounded: 8),
                      width: 50)
                .bordered(.random())
                .add(to: view)
                .centered(on: view, offset: .init(x: 40, y: -180))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .square(),
                      width: 50)
                .add(to: view)
                .centered(on: view, offset: .init(x: 120, y: -180))
            
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .rect(rounded: 8),
                      width: 50,
                      filter: .color(.orange, alpha: 0.3))
                .bordered(.black, width: 3)
                .add(to: view)
                .centered(on: view, offset: .init(x: -120, y: -100))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .square(rounded: 3),
                      shadow: .colored(color: .systemTeal, opacity: 0.5, offset: .init(width: -3, height: -3)),
                      width: 50,
                      filter: .color(.orange, alpha: 0.3))
                .add(to: view)
                .centered(on: view, offset: .init(x: -40, y: -100))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .circle,
                      shadow: .colored(color: .red, opacity: 1, radius: 2),
                      width: 50)
                .add(to: view)
                .centered(on: view, offset: .init(x: 40, y: -100))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"),
                      shaped: .custom(path: triangle(rect: CGRect(origin: .zero, size: .init(width: 50, height: 50)), cornerRadius: 3)),
                      width: 50)
                .add(to: view)
                .centered(on: view, offset: .init(x: 120, y: -100))
                .filtered(color: .random(), alpha: 0.5)
    
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"))
                .add(to: view)
                .centered(on: view, offset: .init(x: -80, y: 0))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"), shaped: .square(rounded: 15), filter: .color(.color(for: "manuelmeyer".avatarHash)))
                .bordered(.color(for: "manuelmeyer".avatarHash), width: 2)
                .add(to: view)
                .centered(on: view, offset: .init(x: -80, y: 140))
            
            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"), shaped: .custom(path: triangle(rect: CGRect(origin: .zero, size: .init(width: 120, height: 120)), cornerRadius: 8)))
                .add(to: view)
                .centered(on: view, offset: .init(x: 80, y: 0))

            Avatar
                .from(image: .init(imageLiteralResourceName: "m1"), shaped: .square(rounded: 15), filter: .color(.random()))
                .bordered(.random(alpha: 1), width: 2)
                .add(to: view)
                .centered(on: view, offset: .init(x: 80, y: 140))
        }
    }

}

fileprivate
func triangle(rect: CGRect, cornerRadius: CGFloat, rotation: CGFloat = 54) -> UIBezierPath {
    let path = UIBezierPath()
    let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
    let r = rect.width / 2
    let rc = cornerRadius
    let rn = r * 0.95 - rc
    
    var cangle = rotation
    for i in 1 ... 5 {
        // compute center point of tip arc
        let cc = CGPoint(x: center.x + rn * cos(cangle * .pi / 180), y: center.y + rn * sin(cangle * .pi / 180))
        
        // compute tangent point along tip arc
        let p = CGPoint(x: cc.x + rc * cos((cangle - 72) * .pi / 180), y: cc.y + rc * sin((cangle - 72) * .pi / 180))
        
        if i == 1 {
            path.move(to: p)
        } else {
            path.addLine(to: p)
        }
        
        // add 144 degree arc to draw the corner
        path.addArc(withCenter: cc, radius: rc, startAngle: (cangle - 72) * .pi / 180, endAngle: (cangle + 72) * .pi / 180, clockwise: true)
        
        cangle += 144
    }
    
    path.close()
    return path
}
