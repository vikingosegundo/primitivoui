//
//  ViewController.swift
//  PrimitivoUI
//
//  Created by Manuel Meyer on 12.02.21.
//

import UIKit
import Primitivo
import AvatarKit

extension EnumeratedSequence {
    func array() -> [Element] { Array(self) }
}

extension String {
    var avatarHash: Int {
        self.data(using: .utf8)!
            .enumerated()
            .array()
            .reduce(173961102589771) { (r, c) -> Int in (r >> 3) + Int(c.element) * 173961102589771 }
    }
}

public
struct User: Identifiable {
    public var id: UUID = UUID()
    var avatarHash: Int { id.uuidString.avatarHash }
    let name: String
    let image: UIImage?
}

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let ip = selected {
            tableView.deselectRow(at: ip, animated: animated)
            selected = nil
        }
    }
    
    private var selected: IndexPath?
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int { 1 }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { 5 }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        switch indexPath.row {
        case 0: c.textLabel?.text = "Avatars"
        case 1: c.textLabel?.text = "Identicons"
        case 2: c.textLabel?.text = "Avatar View"
        case 3: c.textLabel?.text = "Toasts/HUDs"
        case 4: c.textLabel?.text = "ContextMenu"

        default: break
        }
        return c
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: performSegue(withIdentifier: "avatarContactSheet", sender: self)
        case 1: performSegue(withIdentifier: "identiconContactSheet", sender: self)
        case 2: performSegue(withIdentifier: "avatarDetail", sender: self)
        case 3: performSegue(withIdentifier: "toastContactSheet", sender: self)
        case 4: performSegue(withIdentifier: "contextMenu", sender: self)

        default: break
        }
        selected = indexPath
    }
    
}

//                (r >> 1) + (Int(next.element) << next.offset) * 2654435769
//                (r >> 1) + (Int(next.element) >> (next.offset >> 1)) * 2654435769
//                (r >> 1) + (Int(next.element) >> (next.offset >> 1)) * 173961102589771
//                (r >> 2) + (Int(next.element) >> (next.offset >> 1)) * 173961102589771
//                (r >> 3) + (Int(next.element) >> (next.offset >> 1)) * 173961102589771
//                (r >> 3) + (Int(next.element) >> (next.offset >> 1)) * (2654435769)
//                (r >> 3) + (Int(next.element) >> (next.offset >> 1)) * (2654435769 << 2)
//                (r >> 3) + (Int(next.element) >> 2) * (2654435769 << 2)
//                (r >> 3) + (Int(next.element) >> 4) * (2654435769 << 2)
//           r + ((r >> 3) + (Int(c.element) >> 4) * (2654435769 << 2))
